# 	MODULO ADMINISTRACION DE USUARIOS #

En este repositorio se encuentra el módulo de administración de usuarios de acuerdo a la prueba técnica planteada

### Carpetas ###

* BackSpringBoot = Allí se encuentra el código fuente comprimido de la aplicación construida en Java con Spring Boot
* BaseDatos = Aquí se encuentra la estructura de base de datos con la creación de tablas en la base de datos postgres
* CasoDeUso = Aquí se encuentra el planteamiento del problema y el requerimiento
* FrontAngular = Aquí se encuentra el módulo construido a nivel de capa de presentación en angular
* VideoPruebasUnitarias = Aquí se encuentra el video con las pruebas unitarias.