-- Database: restitucion

-- DROP DATABASE restitucion;

CREATE DATABASE restitucion
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
	
-- Table: public.parametro

-- DROP TABLE public.parametro;

CREATE TABLE IF NOT EXISTS public.parametro
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    tipo character varying COLLATE pg_catalog."default",
    valor character varying COLLATE pg_catalog."default",
    estado bigint,
    CONSTRAINT parametro_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.parametro
    OWNER to postgres;
	
	-- Table: public.usuarios

-- DROP TABLE public.usuarios;

CREATE TABLE IF NOT EXISTS public.usuarios
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    primer_nombre character varying(100) COLLATE pg_catalog."default",
    segundo_nombre character varying(100) COLLATE pg_catalog."default",
    primer_apellido character varying(100) COLLATE pg_catalog."default",
    segundo_apellido character varying(100) COLLATE pg_catalog."default",
    numero_documento bigint,
    tipo_documento character varying(100) COLLATE pg_catalog."default",
    genero character varying(50) COLLATE pg_catalog."default",
    nombre_usuario character varying(100) COLLATE pg_catalog."default",
    email character varying(100) COLLATE pg_catalog."default",
    fecha_creacion date,
    usuario_creacion character varying(100) COLLATE pg_catalog."default",
    CONSTRAINT usuarios_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.usuarios
    OWNER to postgres;